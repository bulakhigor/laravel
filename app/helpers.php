<?php

if(!function_exists('debug')){

    /**
     * @param $name
     */
    function debug($name)
    {
        echo "<pre>";
        print_r($name);
        echo "</pre>";
    }
}

if(!function_exists('formatDate')){

    /**
     * @param null $timestamp
     *
     * @return false|string
     */
    function formatDate($timestamp = null)
    {
        return date('d.m.Y H:i:s', $timestamp ?? time());
    }
}

if(!function_exists('anchor')){

    function anchor($link, $title, $text)
    {
        $domain = env('APP_URL');

        $anchor = '<a href="' . $domain . $link . '" ';

        if($title){
            $anchor .= 'title="' . $title . '">' . $text . '</a>';
        }else{
            $anchor .= '>' . $text . '</a>';
        }

        return $anchor;
    }

}