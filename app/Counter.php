<?php
/**
 * Created by PhpStorm.
 * User: igorbulakh
 * Date: 04.01.2018
 * Time: 4:17
 */

namespace App;

class Counter implements CounterInterface
{
    protected $counter = 0;

    public function __construct()
    {
        $this->counter = 0;
    }

    /**
     * @return int
     */
    public function increment()
    {
        return ++$this->counter;
    }

    /**
     * @return int
     */
    public function decrement()
    {
        return --$this->counter;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->counter;
    }
}