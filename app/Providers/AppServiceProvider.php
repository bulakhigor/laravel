<?php

namespace App\Providers;

use App\Classes\PairTag;
use App\Classes\SingleTag;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\CounterInterface', 'App\Counter');

        $this->app->bind('SingleTag', function($name){
            return new SingleTag($name);
        });

        $this->app->bind('PairTag', function($name){
            return new PairTag($name);
        });
    }
}
