<?php

namespace App\Http\Controllers;

use App\CounterInterface;
use App\Models\Customer;
use Illuminate\Http\Request;

class MainController extends Controller
{
    protected $counter;
    protected $request;

    public function __construct(Request $request, CounterInterface $counter)
    {
        $this->counter = $counter;
        $this->request = $request;
    }


    public function index()
    {
        $posts = [];

        return view('layouts.all_posts', [
            'page' => 'pages.posts',
            'title' => 'Future Imperfect by HTML5 UP',
            'posts' => $posts
        ]);
    }

    public function about()
    {

    }

    public function feedback()
    {

    }

    public function test()
    {

    }

    public function db(Request $request)
    {
        $sql = "SELECT * FROM users";
        $data = DB::query($sql);

        foreach($data as $row){
            echo $row[0], $row['username'];
        }
    }

    public function orm()
    {
        /*$customerModel = new Customer();

        $customerModel->name = 'Игорь';
        $customerModel->surname = 'Булах';
        $customerModel->age = 31;
        $customerModel->birthdate = '1987-01-07';
        $customerModel->notes = 'Программист';

        $customerModel->save();*/

        /*$newModel = Customer::create([
            'name' => 'Igor',
            'surname' => 'Bulakh',
            'age' => 31,
            'birthdate' => '1987-01-07',
            'notes' => 'Programmer'
        ]);*/

        $myModel = Customer::find(1);

        $allModels = Customer::all();

        $models1 = Customer::where('name', '=', 'Игорь')->get();

        $models2 = Customer::where('surname', '=', 'Bulakh')->get();

        return 'OK';
    }
}
