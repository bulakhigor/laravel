<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function register()
    {
        return view('layouts.one_post', [
            'page' => 'pages.signup',
            'title' => 'Регистрация'
        ]);
    }

    public function registerPost(RegisterRequest $request)
    {
        DB::table('users')->insert([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('pass'))
            ]);

        return redirect()->intended('/');
    }

    public function login()
    {
        return view('layouts.one_post', [
            'page' => 'pages.login',
            'title' => 'Вход'
        ]);
    }

    public function loginPost(Request $request)
    {
        $login = $request->input('login');
        $pass = $request->input('pass');

        if(Auth::attempt(['email' => $login, 'password' => $pass ])){

            return redirect()->intended('/');

        }else{
            return redirect()->back('/login');
        }
    }
}
