<?php

namespace App\Http\Controllers;

use App\CounterInterface;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $counter;
    protected $request;

    public function __construct(Request $request, CounterInterface $counter)
    {
        $this->counter = $counter;
        $this->request = $request;
    }


    public function index()
    {
        $posts = [];

        return view('layouts.all_posts', [
            'page' => 'pages.posts',
            'title' => 'Future Imperfect by HTML5 UP',
            'posts' => $posts
        ]);
    }


    public function one()
    {
        return view('layouts.one_post', [
            'page' => 'pages.post',
            'title' => 'Future Imperfect by HTML5 UP'
        ]);
    }


    public function add()
    {
        return view('posts.addpost');
    }

    public function addPost()
    {
        return redirect()->route('post.one');
    }

    public function edit()
    {
        return view('posts.editpost');
    }

    public function editPost()
    {
        return redirect()->route('post.one');
    }

    public function delete($id)
    {
        return 'Deleting post ' . $id;
    }
}
