<?php
/**
 * Created by PhpStorm.
 * User: igorbulakh
 * Date: 04.01.2018
 * Time: 12:14
 */

namespace App\Classes;


abstract class Tag
{
    protected $name;
    protected $attributes = [];
    protected $pattern = "<%name% %attr%>";

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function attr($name, $value)
    {
        $this->attributes[$name] = $value;
        return $this;
    }

    public function render()
    {
        $tag = str_replace('%name%', $this->name, $this->pattern);
        $pairs = [];
        if(count($this->attributes) === 0){
            $str_pair = '';
        }else{
            foreach($this->attributes as $key => $value){
                $pairs[] = "$key=\"$value\"";
            }

            $str_pair = ' ' . implode(' ', $pairs);
        }

        $tag = str_replace('%attr%', $str_pair, $tag);

        return $tag;
    }
}