<?php
/**
 * Created by PhpStorm.
 * User: igorbulakh
 * Date: 04.01.2018
 * Time: 4:18
 */

namespace App;

interface CounterInterface
{
    public function increment();
    public function decrement();
    public function getValue();
}