<!DOCTYPE html>
<html lang="ru">
<head>
    <title>{{ $title or '' }}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="../css/style.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="../css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="../css/ie8.css" /><![endif]-->
    @yield('head_styles')

    <!--[if lte IE 8]><script src="../js/ie/html5shiv.js"></script><![endif]-->
    @yield('head_scripts')
</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

    @yield('header')

    @yield('menu-block')

    @yield('content')

    @yield('footer')

    @yield('sidebar')

</div>

    @yield('footer_scripts')

</body>
</html>