@extends('layouts.post')

@section('onepost')
    @include($page)
@endsection

@section('footer_scripts')
    @include('parts.footer_scripts')
@endsection