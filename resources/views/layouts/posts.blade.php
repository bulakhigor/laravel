@extends('layouts.mainlayout')

@section('header')
    @include('parts.header')
@endsection

@section('menu-block')
    @include('parts.menu-block')
@endsection

@section('content')
    <div id="main">

        @yield('mainpost')

    </div>
@endsection

