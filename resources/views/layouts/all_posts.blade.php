@extends('layouts.posts')

@section('mainpost')
    @include($page)
@endsection

@section('footer_scripts')
    @include('parts.footer_scripts')
@endsection

@section('sidebar')
    <section id="sidebar">
        @include('widgets.intro')
        @include('widgets.mini_posts')
        @include('widgets.post_list')
        @include('widgets.about')
        @include('widgets.footer')
    </section>
@endsection