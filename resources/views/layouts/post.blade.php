@extends('layouts.mainlayout')

@section('header')
    @include('parts.header')
@endsection

@section('head_styles')
    @include('parts.head_styles')
@endsection

@section('menu-block')
    @include('parts.menu-block')
@endsection

@section('content')
    <div id="main">

        @yield('onepost')

    </div>
@endsection

@section('footer')
    @include('parts.footer_onepost')
@endsection