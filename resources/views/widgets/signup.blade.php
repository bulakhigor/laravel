<form method="POST" class="sign-up" action="{{ route('auth.signup.post') }}">
    {{ csrf_field() }}
    <div class="line">
        <label>
            <span>Имя:</span>
            <input name="name" type="text" placeholder="Введите Ваше имя" class="login-input" value="{{ old('name') }}">
            @if($errors->first('name'))
                <span class="auth-error">{{ $errors->first('name') }}</span>
            @endif
        </label>
    </div>
    <div class="line">
        <label>
            <span>Email:</span>
            <input name="email" type="email" placeholder="Введите Ваш Email" value="{{ old('email') }}">
            @if($errors->first('email'))
                <span class="auth-error">{{ $errors->first('email') }}</span>
            @endif
        </label>
    </div>
    <div class="line">
        <label>
            <span>Пароль:</span>
            <input name="pass" type="password" placeholder="Введите пароль" value="{{ old('pass') }}">
            @if($errors->first('pass'))
                <span class="auth-error">{{ $errors->first('pass') }}</span>
            @endif
        </label>
    </div>
    <div class="line">
        <label>
            <span>Пароль:</span>
            <input name="pass_confirm" type="password" placeholder="Повторите пароль" value="{{ old('pass_confirm') }}">
            @if($errors->first('pass_confirm'))
                <span class="auth-error">{{ $errors->first('pass_confirm') }}</span>
            @endif
        </label>
    </div>
    <div class="line">
        <label>
            <input type="submit" value="Зарегистрироваться">
        </label>
    </div>
</form>