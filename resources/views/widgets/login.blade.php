<form method="POST" class="sign-in">
    {{ csrf_field() }}
    <div class="line">
        <label>
            <span>Логин:</span>
            <input name="login" type="text" placeholder="Введите логин">
        </label>
    </div>
    <div class="line">
        <label>
            <span>Пароль:</span>
            <input name="pass" type="password" placeholder="Введите пароль">
        </label>
    </div>
    <div class="line">
        <label>
            <span>Запомнить:</span>
            <input name="remember" type="checkbox">
        </label>
    </div>
    <div class="line">
        <label>
            <input type="submit" value="Войти">
        </label>
    </div>
</form>