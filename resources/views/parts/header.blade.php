<header id="header">
    <h1><a href="{{ route('site.main.index') }}">Future Imperfect</a></h1>
    <nav class="links">
        <ul>
            <li><a href="{{ route('site.main.index') }}">Главная</a></li>
            <li><a href="{{ route('site.test') }}">Тест</a></li>
            <li><a href="{{ route('site.main.about') }}">Обо мне</a></li>
            <li><a href="{{ route('site.main.feedback') }}">Обратная связь</a></li>
        </ul>
    </nav>
    <nav class="main">
        <ul>
            <li class="search">
                <a class="fa-search" href="#search">Поиск</a>
                <form id="search" method="get" action="#">
                    <input type="text" name="query" placeholder="Поиск" />
                </form>
            </li>
            <li class="menu">
                <a class="fa-bars" href="#menu">Меню</a>
            </li>
        </ul>
    </nav>
</header>