<script src="../js/jquery.min.js"></script>
<script src="../js/skel.min.js"></script>
<script src="../js/util.js"></script>
<!--[if lte IE 8]><script src="../js/ie/respond.min.js"></script><![endif]-->
<script src="../js/main.js"></script>
<script>
    $(function () {
        let inputs = $('.line input');

        if(inputs.next().hasClass('auth-error')){
            let error = inputs.next();
            error.prev().addClass('auth-error');
        }else{
            if(inputs.hasClass('auth-error')) {
                inputs.removeClass('auth-error');
            }
        }
    });
</script>