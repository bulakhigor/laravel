<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('', 'PostController@index')->name('post.all');
Route::get('', 'MainController@index')->name('site.main.index');

Route::get('test', 'MainController@test')->name('site.test');

Route::get('about', 'MainController@about')->name('site.main.about');

Route::get('feedback', 'MainController@feedback')->name('site.main.feedback');







Route::get('post/{id}', 'PostController@one')->where('id', '[0-9]+')->name('post.one');

Route::get('post/add', 'PostController@add')->name('post.add');

Route::post('post/add', 'PostController@addPost')->name('post.add.post');

Route::get('post/edit/{id}', 'PostController@edit')->where('id', '[0-9]+')->name('post.edit');

Route::post('post/edit/{id}', 'PostController@editPost')->where('id', '[0-9]+')->name('post.edit.post');

Route::get('delete/{id}', 'PostController@delete')->where('id', '[0-9]+')->name('post.delete');





Route::get('login', 'AuthController@login')->name('auth.login');

Route::post('login', 'AuthController@loginPost')->name('auth.login.post');

Route::get('signup', 'AuthController@register')->name('auth.signup');

Route::post('signup', 'AuthController@registerPost')->name('auth.signup.post');




Route::get('404', function() {
    return view('errors.404');
})->middleware('logging');




Route::get('orm', 'MainController@orm')->name('site.test.orm');